import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styles: [`
    .products li { cursor: default; }
    .products li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class ProductsComponent implements OnInit {

 products;

 currentProduct;

 isLoading = true;

  constructor(private _productsService: ProductsService) {
    //this.users = this._userService.getUsers();
  }
 select(product){
		this.currentProduct = product; 
    //console.log(	this.currentUser);
 }
  
  deleteProduct(product){
      this._productsService.deleteProduct(product);
  }

  addProduct(product){
    this._productsService.addProduct(product);
  }

  editProduct(product){
     this._productsService.updateProduct(product); 
  }  
  


  ngOnInit() {
        this._productsService.getProducts()
			    .subscribe(products => {this.products = products;
                               this.isLoading = false;
                               console.log(products)});
  }

}