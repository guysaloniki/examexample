import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
  private _url = "http://jsonplaceholder.typicode.com/users";
  productsObservable;

  constructor(private af:AngularFire) { }
  
  addProduct(product){
    this.productsObservable.push(product);
  }

  deleteProduct(product){
    this.af.database.object('/products/' + product.$key).remove();
    console.log('/products/' + product.$key);
  }

  updateProduct(product){
    let product1 = {pid:product.pid,pname:product.pname,cost:product.cost,categoryid:product.categoryid}
    console.log(product1);
    this.af.database.object('/products/' + product.$key).update(product1)
  }  

  getProducts(){
    this.productsObservable = this.af.database.list('/products').map(
      products =>{
        products.map(
          product => {
            product.posTitles = [];
            for(var p in product.categorys){
                product.posTitles.push(
                this.af.database.object('/categorys/' + p)
              )
            }
          }
        );
        return products;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.productsObservable;
	}
}