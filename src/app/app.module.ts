import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import{AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {PostsComponent} from './posts/posts.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';

import { UsersService } from './users/users.service';
import { ProductsService } from './products/products.service';
import { UserFormComponent } from './user-form/user-form.component';
import { FireComponent } from './fire/fire.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';

export const firebaseConfig = {
    apiKey: "AIzaSyDRIWeO6UpII56K4aSB0TNlgog_To39pxw",
    authDomain: "test01-3b2ff.firebaseapp.com",
    databaseURL: "https://test01-3b2ff.firebaseio.com",
    storageBucket: "test01-3b2ff.appspot.com",
    messagingSenderId: "138017194426"
}



const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'fire', component: FireComponent },
  { path: '', component: UsersComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    PostsComponent,
    UserComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    FireComponent,
    ProductsComponent,
    ProductComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
