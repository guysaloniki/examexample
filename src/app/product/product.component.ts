import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product'


@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {

  @Output() deleteEvent = new EventEmitter<Product>();
  @Output() editEvent = new EventEmitter<Product>();

  product:Product;
  tempProduct:Product = {pid:null,pname:null,cost:null,categoryid:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';
  
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.product);
  }

  cancelEdit(){
    this.isEdit = false;
    this.product.pid = this.tempProduct.pid;
   this.product.pname = this.tempProduct.pname;
   this.product.cost = this.tempProduct.cost;
   this.product.categoryid = this.tempProduct.categoryid;
    this.editButtonText = 'Edit'; 
  }

  toggleEdit(){
    //update parent about the change
    this.isEdit = !this.isEdit; 
    this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     
     if(this.isEdit){
       this.tempProduct.pid = this.product.pid;
       this.tempProduct.pname = this.product.pname;
       this.tempProduct.cost = this.product.cost;
       this.tempProduct.categoryid = this.product.categoryid;

     } else {     
       this.editEvent.emit(this.product);
     } 
  }

  ngOnInit() {
  }

}
